package com.epam.rd.khranovskyi.web.boot.controller;

import com.epam.rd.khranovskyi.db.entity.Client;
import com.epam.rd.khranovskyi.web.boot.service.ClientService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/users")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping(path = "/{id}")
    public Client getClientById(@PathVariable Long id) {
        return clientService.getById(id);
    }

    @GetMapping
    public List<Client> getAll() {
        return clientService.getAll();
    }

    @GetMapping(path = "/searchBy")
    public List<Client> getClientByName(@RequestParam String name) {
        return clientService.getByName(name);
    }

    @PostMapping(path = "/registration")
    public Client createClient(){
        Client client = new Client();
        client.setName(RandomString.make(5));
        client.setMail(RandomString.make(5)+"@mail.com");
        client.setBalance((int)(5+Math.random()*100));
        clientService.save(client);
        return client;
    }
}
