package com.epam.rd.khranovskyi.web.boot.metrics;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component("myHealthCheck")
public class HealthCheck implements HealthIndicator {

    @Override
    public Health health() {
        return Health.down()
                .withDetail("Project Name", "BootExample").build();

    }
}
