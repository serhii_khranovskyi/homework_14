package com.epam.rd.khranovskyi.web.boot.service;

import com.epam.rd.khranovskyi.db.entity.Client;

import java.util.List;

public interface ClientService {
    List<Client> getByName(String name);

    List<Client> getAll();

    Client getById(Long id);

    void save(Client client);
}
